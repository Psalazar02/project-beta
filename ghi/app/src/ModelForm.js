import React, {useEffect, useState} from "react";

function ModelForm() {
    const [name, setName] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [manufacturerId, setManufacturerId] = useState("");
    const [manufacturers, setManufacturers] = useState([]);

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    };
    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    };
    const handleManufacturerIdChange = (e) => {
        const value = e.target.value;
        setManufacturerId(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturerId;

        const modelUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        console.log("response", response)
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturerId('');
        };
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                <h1>Add a model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                <div className='form-floating mb-3'>
                    <input value={name} onChange={handleNameChange} placeholder='Name' required type='text' name='name' id='name' className='form-control'/>
                    <label htmlFor='name'>Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className='mb-3'>
                <select value={manufacturerId} onChange={handleManufacturerIdChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                        <option>Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <button className='btn btn-primary'>Create</button>
                </form>
                </div>
        </div>
        </div>
        </>
    )
}
export default ModelForm;
