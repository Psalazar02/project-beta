import { useEffect, useState } from "react";

function ModelsList() {
    const [models, setModels] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="container">
            <h1 className="mt-4">List Models</h1>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>
                                    <img alt={model.name} src={ model.picture_url } height={200} className="photo .img-thumbnail" />
                                </td>
                                <td>{ model.name}</td>
                                <td>{ model.manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ModelsList;
