import { NavLink } from 'react-router-dom';

function Nav() {
  return (
<nav className="navbar navbar-expand-lg bg-body-tertiary navbar-dark bg-success">
  <div className="container">
  <NavLink className="navbar-brand" to="/">CarCar</NavLink>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNavDropdown">
      <ul className="navbar-nav">

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Manufacturers
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/manufacturers">List Manufacturers</NavLink></li>
            <li><NavLink className="dropdown-item" to="/manufacturers/new">Add Manufacturer</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Models
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/models">List Models</NavLink></li>
            <li><NavLink className="dropdown-item" to="/models/new">Add Model</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Inventory
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/inventory">List Automobiles</NavLink></li>
            <li><NavLink className="dropdown-item" to="/inventory/new">Add Automobile</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Technicians
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/technicians">List Technicians</NavLink></li>
            <li><NavLink className="dropdown-item" to="/technicians/new">Add Technician</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Service Appointments
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/service-appointments">List Service Appointments</NavLink></li>
            <li><NavLink className="dropdown-item" to="/service-appointments/new">Add Service Appointment</NavLink></li>
            <li><NavLink className="dropdown-item" to="/service-appointments/history">List Service History / Search VIN</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Salespeople
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/salespeople">List Salespeople</NavLink></li>
            <li><NavLink className="dropdown-item" to="/salespeople/new">Add Salesperson</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Customers
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/customers">List Customers</NavLink></li>
            <li><NavLink className="dropdown-item" to="/customers/new">Add Customer</NavLink></li>
          </ul>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Sales
          </a>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/sales/">List Sales</NavLink></li>
            <li><NavLink className="dropdown-item" to="/sales/new">New Sale</NavLink></li>
            <li><NavLink className="dropdown-item" to="/sales/history">Sales History</NavLink></li>
          </ul>
        </li>


      </ul>
    </div>
  </div>
</nav>
  )
}

export default Nav;
