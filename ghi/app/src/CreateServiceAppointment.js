import { useEffect, useState } from 'react';
import './index.css';

function CreateServiceAppointment() {
    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: '',
    })

    const getTechnicians = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      }

      useEffect(()=> {
        getTechnicians();
      }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const serviceAppointmentUrl = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(serviceAppointmentUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setFormData({
                vin: '',
                customer: '',
                date_time: '',
                technician: '',
                reason: '',
            });
        }
    };

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
        }

    return (
        <div className="row mt-4">
            <div className="col-10 col-lg-6">
                <h1>Add a Service Appointment</h1>
                <form onSubmit={handleSubmit}>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Customer Name" type="text" id="customer" name="customer" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Date and Time" type="datetime-local" id="date_time" name="date_time" className="form-control" />
                    </div>
                    <div className="p-2">
                        <select onChange={handleChangeInput} required name="technician" className="form-select" id="technician">
                        <option value="">Choose a technician</option>
                        {technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                        )
                        })}
                        </select>
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Reason" type="text" id="reason" name="reason" className="form-control" />
                    </div>
                    <button className="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    );
}

export default CreateServiceAppointment;
