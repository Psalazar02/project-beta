import { useEffect, useState } from 'react';
import './index.css';

function ListTechnicians() {
    const [technicians, setTechnicians] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(()=>{
        getData()
      }, [])

      return (
        <div className="mt-4">
            <h1>List Technicians</h1>
            <table className="table mt-4">
                <thead>
                    <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => {
                    return (
                        <tr key={technician.id}>
                        <td>{ technician.employee_id }</td>
                        <td>{ technician.first_name }</td>
                        <td>{ technician.last_name }</td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ListTechnicians;
