import { useEffect, useState } from 'react';
import './index.css';

function ListInventoryAutos() {
    const [autos, setAutos] = useState([])


    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    useEffect(()=>{
        getData()
      }, [])

      return (
        <div className="row mt-4">
          <div className="col-12 col-lg-10">
            <h1>List Automobiles</h1>
            <table className="table mt-4">
              <thead>
                <tr>
                  <th>VIN</th>
                  <th>Color</th>
                  <th>Year</th>
                  <th>Model</th>
                  <th>Manufacturer</th>
                  <th>Sold</th>
                </tr>
              </thead>
              <tbody>
                {autos.map(auto => {
                  return (
                    <tr key={auto.href}>
                      <td>{auto.vin}</td>
                      <td>{auto.color}</td>
                      <td>{auto.year}</td>
                      <td>{auto.model.name}</td>
                      <td>{auto.model.manufacturer.name}</td>
                      <td>{auto.sold ? 'Sold' : 'Not Sold'}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
      </div>
    );

}

export default ListInventoryAutos;
