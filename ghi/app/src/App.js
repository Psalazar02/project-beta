import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelsList from './ModelsList'
import ModelForm from './ModelForm'
import ListManufacturers from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import CreateInventoryAuto from './CreateInventoryAuto';
import ListSalespeople from './ListSalespeople';
import SalespersonForm from './SalespersonForm';
import ListCustomers from './ListCustomers';
import CustomerForm from './CustomerForm';
import ListInventoryAutos from './ListInventoryAutos';
import ListTechnicians from './ListTechnicians';
import CreateTechnician from './CreateTechnician';
import ListServiceAppointments from './ListServiceAppointments';
import CreateServiceAppointment from './CreateServiceAppointment';
import ServiceHistory from './ServiceHistory';
import SaleForm from './CreateSaleForm';
import ListSales from './ListSales';
import SalespersonSaleHistory from './SalespersonSalesHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='models'>
            <Route index element={<ModelsList />} />
            <Route path='new' element={<ModelForm />} />
          </Route>
          <Route path='salespeople'>
            <Route index element={<ListSalespeople />} />
            <Route path='new' element={<SalespersonForm />} />
          </Route>
          <Route path='customers'>
            <Route index element={<ListCustomers />} />
            <Route path='new' element={<CustomerForm />} />
          </Route>
          <Route path='manufacturers'>
            <Route index element={<ListManufacturers />} />
            <Route path="new" element={<CreateManufacturer />} />
          </Route>
          <Route path='inventory'>
            <Route index element={<ListInventoryAutos />} />
            <Route path="new" element={<CreateInventoryAuto />} />
          </Route>
          <Route path='technicians'>
            <Route index element={<ListTechnicians />} />
            <Route path="new" element={<CreateTechnician />} />
          </Route>
          <Route path='service-appointments'>
            <Route index element={<ListServiceAppointments />} />
            <Route path="new" element={<CreateServiceAppointment />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path='sales'>
            <Route index element={<ListSales />} />
            <Route path='new' element={<SaleForm />} />
            <Route path='history' element={<SalespersonSaleHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
