import { useEffect, useState } from "react";

function SalespersonSaleHistory() {
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesPerson] = useState('')
    const [filteredSales, setFilteredSales] = useState([])

    const getData = async () => {
        const salesResponse = await fetch('http://localhost:8090/api/sales/');
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        };

        const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/')
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        };
    }
    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesPerson(value);
        setFilteredSales(sales.filter(sale => sale.salesperson.id === Number(value)));
    };

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1>Salesperson History</h1>
            <div className="container">
                <div className='mb-3'>
                <select value={salesperson.id} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })};
                </select>
                </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                {filteredSales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}.00</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            </div>
        </div>
    )
}

export default SalespersonSaleHistory;
