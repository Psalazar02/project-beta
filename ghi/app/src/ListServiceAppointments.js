import { useEffect, useState } from 'react';
import './index.css';

function ListServiceAppointments() {

    const [serviceAppointments, setServiceAppointments] = useState([])
    const [vinsList, setVinsList] = useState([]);

    const getData = async () => {

        const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            setServiceAppointments(appointmentData.appointments);
        }

        const automobileResponse = await fetch('http://localhost:8080/api/service/automobile-vo/');
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            const vinsList = automobileData.automobileVOs.map(auto => auto.vin);
            setVinsList(vinsList);
        }
    }

    useEffect(()=>{
        getData()
        }, [])

        const finishServiceAppointment = async(id) => {
            const fetchConfig = {
              method: "put",
              headers: {
                'Content-Type': 'application/json',
              },
            };
            await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchConfig)
            setServiceAppointments(prevAppointments => prevAppointments.filter(prevAppointments => prevAppointments.id !== id));
          };

        const cancelServiceAppointment = async(id) => {
            const fetchConfig = {
              method: "put",
              headers: {
                'Content-Type': 'application/json',
              },
            };
            await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchConfig)
            setServiceAppointments(prevAppointments => prevAppointments.filter(prevAppointments => prevAppointments.id !== id));
          };

        return (
        <div className="mt-4">
            <h1>List Service Appointments</h1>
            <table className="table mt-4">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Change Status</th>
                    </tr>
                </thead>
                <tbody>
                    {serviceAppointments.filter(serviceAppointment => serviceAppointment.status === "created").map(serviceAppointment => {
                    const dateDisplay = new Date(serviceAppointment.date_time).toDateString();
                    const timeDisplay = new Date(serviceAppointment.date_time).toLocaleTimeString();
                    return (
                        <tr key={serviceAppointment.id}>
                            <td>{ serviceAppointment.vin }</td>
                            <td>{ vinsList.includes(serviceAppointment.vin) ? 'Yes' : 'No' }</td>
                            <td>{ serviceAppointment.customer }</td>
                            <td>{ dateDisplay }</td>
                            <td>{ timeDisplay }</td>
                            <td>{ serviceAppointment.technician.first_name } { serviceAppointment.technician.last_name }</td>
                            <td>{ serviceAppointment.reason }</td>
                            <td>
                                <button className="btn m-1 btn-sm btn-primary" type="button" id={serviceAppointment.id} onClick={() => finishServiceAppointment(serviceAppointment.id)}>Finish</button>
                                <button className="btn m-1 btn-sm btn-primary" type="button" id={serviceAppointment.id} onClick={() => cancelServiceAppointment(serviceAppointment.id)}>Cancel</button>
                            </td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ListServiceAppointments;
