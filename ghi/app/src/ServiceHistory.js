import { useEffect, useState } from 'react';
import './index.css';

function ServiceHistory() {

    const [serviceAppointments, setServiceAppointments] = useState([])
    const [filteredServiceAppointments, setFilteredServiceAppointments] = useState([])
    const [vinsList, setVinsList] = useState([]);
    const [formData, setFormData] = useState({
        search: '',
    })

    const getData = async () => {

        const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            setServiceAppointments(appointmentData.appointments);
        }

        const automobileResponse = await fetch('http://localhost:8080/api/service/automobile-vo/');
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            const vinsList = automobileData.automobileVOs.map(auto => auto.vin);
            setVinsList(vinsList);
        }
    }

    useEffect(()=>{
        getData()
        }, [])

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
        }

        const handleSubmit = async (event) => {
            event.preventDefault();
            const searchedVin = formData.search;
            const filteredServiceAppointments = serviceAppointments.filter(
                serviceAppointment => serviceAppointment.vin === searchedVin
            );
            setFilteredServiceAppointments(filteredServiceAppointments);
        };

        const resetList = async (event) => {
            setFilteredServiceAppointments([]);
        };

        return (
        <div className="mt-4">
            <h1>List Service History / Search VIN</h1>
            <form className="d-flex mb-4 align-items-end" onSubmit={handleSubmit}>
                <div className="p-2">
                    <label className="pb-2" htmlFor="search">Search by VIN:</label>
                    <input onChange={handleChangeInput} required placeholder="VIN" type="text" id="search" name="search" className="form-control" />
                </div>
                <div>
                    <button className="btn btn-primary mb-2 mx-2">Search</button>
                </div>
                <div>
                    <button className="btn btn-primary mb-2 mx-2" type="button" onClick={() => resetList()}>Show all</button>
                </div>
            </form>
            <table className="table mt-4">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {filteredServiceAppointments.length > 0 ? (
                    filteredServiceAppointments.map(serviceAppointment => {
                        const dateDisplay = new Date(serviceAppointment.date_time).toDateString();
                        const timeDisplay = new Date(serviceAppointment.date_time).toLocaleTimeString();
                        return (
                            <tr key={serviceAppointment.id}>
                                <td>{ serviceAppointment.vin }</td>
                                <td>{ vinsList.includes(serviceAppointment.vin) ? 'Yes' : 'No' }</td>
                                <td>{ serviceAppointment.customer }</td>
                                <td>{ dateDisplay }</td>
                                <td>{ timeDisplay }</td>
                                <td>{ serviceAppointment.technician.first_name } { serviceAppointment.technician.last_name }</td>
                                <td>{ serviceAppointment.reason }</td>
                                <td>{ serviceAppointment.status }</td>
                            </tr>
                        );
                        })
                    ) : (
                        serviceAppointments.map(serviceAppointment => {
                        const dateDisplay = new Date(serviceAppointment.date_time).toDateString();
                        const timeDisplay = new Date(serviceAppointment.date_time).toLocaleTimeString();
                        return (
                            <tr key={serviceAppointment.id}>
                                <td>{ serviceAppointment.vin }</td>
                                <td>{ vinsList.includes(serviceAppointment.vin) ? 'Yes' : 'No' }</td>
                                <td>{ serviceAppointment.customer }</td>
                                <td>{ dateDisplay }</td>
                                <td>{ timeDisplay }</td>
                                <td>{ serviceAppointment.technician.first_name } { serviceAppointment.technician.last_name }</td>
                                <td>{ serviceAppointment.reason }</td>
                                <td>{ serviceAppointment.status }</td>
                            </tr>
                        );
                        })
                    )
                }
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
