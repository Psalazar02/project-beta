import React, { useEffect, useState } from "react";

function SaleForm() {
    const [price, setPrice] = useState("")
    const [vin, setVin] = useState("")
    const [automobiles, setAutomobiles] = useState([])
    const [salesperson, setSalesperson] = useState("")
    const [salespeople, setSalespeople] = useState([])
    const [customer, setCustomer] = useState("")
    const [customers, setCustomers] = useState([])

    const handlePriceChange = (e) => {
        const value = e.target.value;
        setPrice(value);
    };
    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    };
    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value);
    };
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();


        const data = {};
        data.salesperson = salesperson;
        data.automobile = vin;
        data.price = price;
        data.customer = customer;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            setPrice("");
            setVin("");
            setSalesperson("");
            setCustomer("");
        };
    };

    const fetchData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const salespersonResponse = await fetch(salespersonUrl);
        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json();
            setSalespeople(salespersonData.salespeople);
        };
        const vinUrl = "http://localhost:8100/api/automobiles/";
        const vinResponse = await fetch(vinUrl);
        if (vinResponse.ok) {
            const vinData = await vinResponse.json();
            setAutomobiles(vinData.autos);
        };
        const customerUrl = "http://localhost:8090/api/customers/";
        const customerResponse = await fetch(customerUrl);
        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                <h1>Record a new sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                <div className='mb-3'>
                <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                        <option>Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <div className='mb-3'>
                <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                        <option>Customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <div className='mb-3'>
                <select value={vin} onChange={handleVinChange} required name="automobile_vin" id="automobile_vin" className="form-select">
                        <option>Automobile VIN</option>
                        {automobiles.filter(automobile => automobile.sold === false ).map(automobile => {
                            return (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>
                            );
                        })};
                </select>
                </div>
                <div className='form-floating mb-3'>
                    <input value={price} onChange={handlePriceChange} placeholder='price' required type='text' name='price' id='price' className='form-control'/>
                    <label htmlFor='price'>price</label>
                </div>
                <button className='btn btn-primary'>
                    Create
                </button>
                </form>
                </div>
        </div>
        </div>
        </>
    );
}
export default SaleForm;
