import { useState } from 'react';

function ManufacturerForm() {

    const [formData, setFormData] = useState({
        name: ''
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setFormData({
                name: '',
            });
        }
    };

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
          }

    return (
        <div className="row mt-4">
            <div className="col-10 col-lg-5">
                <h1>Add a Manufacturer</h1>
                <form onSubmit={handleSubmit}>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Manufacturer Name" type="text" id="name" name="name" className="form-control" />
                    </div>
                    <button className="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    );

}

export default ManufacturerForm;
