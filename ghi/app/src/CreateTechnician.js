import { useState } from 'react';
import './index.css';

function CreateTechnician() {

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: ''
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const technicianUrl = 'http://localhost:8080/api/technicians/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(technicianUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: ''
            });
        }
    };

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
          }

    return (
        <div className="row mt-4">
            <div className="col-10 col-lg-5">
                <h1>Add a Technician</h1>
                <form onSubmit={handleSubmit}>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Employee ID (Numeric)" type="number" id="employee_id" name="employee_id" className="form-control" />
                    </div>
                    <button className="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    );
}

export default CreateTechnician;
