import { useEffect, useState } from 'react';

function CreateInventoryAuto() {
    const [carModels, setCarModels] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: ''
    })

    const getCarModels = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setCarModels(data.models);
        }
      }

      useEffect(()=> {
        getCarModels();
      }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const autoUrl = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(autoUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: ''
            });
        }
    };

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
          }

    return (
        <div className="row mt-4">
            <div className="col-10 col-lg-6">
                <h1>Add an Automobile</h1>
                <form onSubmit={handleSubmit}>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Year" type="number" id="year" name="year" className="form-control" />
                    </div>
                    <div className="p-2">
                        <input onChange={handleChangeInput} required placeholder="Vin" type="text" id="vin" name="vin" className="form-control" />
                    </div>
                    <div className="p-2">
                        <select onChange={handleChangeInput} required name="model_id" className="form-select" id="model_id">
                        <option value="">Choose a car model</option>
                        {carModels.map(carModel => {
                        return (
                            <option key={carModel.id} value={carModel.id}>{carModel.name}</option>
                        )
                        })}
                        </select>
                    </div>

                    <button className="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    );

}

export default CreateInventoryAuto;
