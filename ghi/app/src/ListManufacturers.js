import { useEffect, useState } from 'react';
import './index.css';

function ListManufacturers() {
    const [manufacturers, setManufacturers] = useState([])


    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(()=>{
        getData()
      }, [])

      return (
        <div>
          <h1 className="mt-4">List Manufacturers</h1>
          <table className="table">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map(manufacturer => {
                return (
                  <tr key={manufacturer.href}>
                    <td>{ manufacturer.name }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );

}

export default ListManufacturers;
