# CarCar

Team:
* Daniel Brainich - Service microservice
* Pedro Salazar-Mondragon - Sales microservice

## How to run this app
You will need Docker, GitLab, and Node.js 18.2 or above.

1. Fork this GitLab project repository: https://gitlab.com/Psalazar02/project-beta
2. Clone your fork to your local computer:
		# git clone <<your_repository_url>>
3. Build and run your docker containers:
		# docker volume create beta-data
 		# docker-compose build
		# docker-compose up
4. Make sure your containers are running.
5. View the project in the browser: http://localhost:3000/

## Diagram
Please see ./CarCarDiagram.png for a project diagram that incudes an overview of our project, including:
1. Microservices
2. Models
3. Relationships

## API Documentation

### URLs and Ports

Below you will find detailed information regarding URLs, ports, and data shape for each API endpoint in the inventory, service, and sales microservices.

### Inventory API
The inventory microservice handles all aspects of the operation related to vehicle inventory. It integrates with the sales and service microservices through the Automobile model, which sends information to the sales and service microservice's AutomobileVO models.

Relevant models include:
1. A Manufacturer model containing a name field. This is a value object.
2. A VehicleModel model containing name, picture_url, and manufacturer fields. This is a value object.
3. An Automobile model containing color, year, vin, sold, and model fields.

API Endpoints:

1. List Manufacturers
Method:         GET
URL:            http://localhost:8100/api/manufacturers/
Explanation:
This endpoint allows you to view a list of manufacturers. An empty GET request will fetch data with the following structure:
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}

2. Create a Manufacturer
Method:         POST
URL:            http://localhost:8100/api/manufacturers/
Explanation:
This endpoint allows you to add a new manufacturer. A POST request with the following data structure will create a new manufacturer:
{
  "name": "Chrysler"
}

3. Get a specific Manufacturer
Method:         GET
URL:            http://localhost:8100/api/manufacturers/:id/
Explanation:
This endpoint allows you to view a specific manufacturer. Set ":id" in the above url to the manufacturer's id. An empty GET request will fetch data with the following structure:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Daimler-Chrysler"
}

4. Update a specific Manufacturer
Method:         PUT
URL:            http://localhost:8100/api/manufacturers/:id/
Explanation:
This endpoint allows you to update a specific manufacturer. Set ":id" in the above url to the manufacturer's id. A PUT request with the following structure will update that manufacturer:
{
	"name": "Ford"
}

5. Delete a specific manufacturer
Method:         DELETE
URL:            http://localhost:8100/api/manufacturers/:id/
Explanation:
This endpoint allows you to delete a specific manufacturer. Set ":id" in the above url to the manufacturer's id. An empty DELETE request will delete that manufacturer.

6. List vehicle models
Method:         GET
URL:            http://localhost:8100/api/models/
Explanation:
This endpoint allows you to view a list of vehicle models. An empty GET request will fetch data with the following structure:
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}

7. Create a vehicle model
Method:         POST
URL:            http://localhost:8100/api/models/
Explanation:
This endpoint allows you to add a new vehicle model. A POST request with the following data structure will create a new vehicle model:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

8. Get a specific vehicle model
Method:         GET
URL:            http://localhost:8100/api/models/:id/
Explanation:
This endpoint allows you to view a specific vehicle model. Set ":id" in the above url to the vehicle model's id. An empty GET request will fetch data with the following structure:
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}

9. Update a specific vehicle model
Method:         PUT
URL:            http://localhost:8100/api/models/:id/
Explanation:
This endpoint allows you to update a specific vehicle model. Set ":id" in the above url to the vehicle model's id. A PUT request with the following structure will update that vehicle model:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

10. Delete a specific vehicle model
Method:         DELETE
URL:            http://localhost:8100/api/models/:id/
Explanation:
This endpoint allows you to delete a specific vehicle model. Set ":id" in the above url to the vehicle model's id. An empty DELETE request will delete that vehicle model.

11. List automobiles
Method:         GET
URL:            http://localhost:8100/api/automobiles/
Explanation:
This endpoint allows you to view a list of automobiles. An empty GET request will fetch data with the following structure:
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}

12. Create a an automobile
Method:         POST
URL:            http://localhost:8100/api/automobiles/
Explanation:
This endpoint allows you to add a new automobile. A POST request with the following data structure will create a new automobile:
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

13. Get a specific automobile
Method:         GET
URL:            http://localhost:8100/api/automobiles/:vin/
Explanation:
This endpoint allows you to view a specific automobile. Set ":vin" in the above url to the automobile's vin. An empty GET request will fetch data with the following structure:
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}

14. Update a specific automobile
Method:         PUT
URL:            http://localhost:8100/api/automobiles/:vin/
Explanation:
This endpoint allows you to update a specific automobile. Set ":vin" in the above url to the automobile's vin. A PUT request with the following structure will update that automobile:
{
  "color": "red",
  "year": 2012,
  "sold": true
}

15. Delete a specific automobile
Method:         DELETE
URL:            http://localhost:8100/api/automobiles/:vin/
Explanation:
This endpoint allows you to delete a specific automobile. Set ":vin" in the above url to the automobile's vin. An empty DELETE request will delete that automobile.

### Service API
The service microservice handles all aspects of the operation related to vehicle repairs and maintenance. It integrates with the inventory microservice by polling for information about automobiles in inventory. It uses that information to check if a car to be serviced ever appeared in our inventory. If yes, the owner of that car is treated as a VIP when they come in for service.

Relevant models include:
1. A Technician model containing first_name, last_name, and employee_id fields.
2. An AutomobileVO model containing vin, and sold fields. This is a value object that polls for data from the Automobile model in the inventory service.
3. An Appointment model containing date_time, reason, status, vin, customer, and technician fields.

API Endpoints:

1. List Technicians
Method:         GET
URL:            http://localhost:8080/api/technicians/
Explanation:
This endpoint allows you to view a list of technicians. An empty GET request will fetch data with the following structure:
{
	"technicians": [
		{
			"first_name": "Frank",    	(str)
			"last_name": "Carman",     	(str)
			"employee_id": 1234      	(int)
			"id": 1               		(int)
		},
	]
}

2. Create a Technician
Method:         POST
URL:            http://localhost:8080/api/technicians/
Explanation:
This endpoint allows you to add a new technicians. A POST request with the following data structure (employee_id must be unique) will create a new technician:
{
	"first_name": "Frank"
	"last_name": "Carman"
	"employee_id": 1234
}

3. Delete a specific technician
Method:         DELETE
URL:            http://localhost:8080/api/technicians/:id/
Explanation:
This endpoint allows you to delete a specific technician. Set ":id" in the above url to the technician's id. An empty DELETE request will delete that technician. (Please use the automatically-generated id and not the custom employee_id.)

4. List appointments
Method: GET
URL:    http://localhost:8080/api/appointments/
Explanation:
This endpoint allows you to view a list of all service appointments. An empty GET request will fetch data with the following structure:
{
	"appointments": [
		{
			"date_time": "2023-12-01T02:30:00+00:00",
			"reason": "Engine light on",
			"status": "finished",
			"vin": "4T1BF3EK8BU168531",
			"customer": "Bob Doe",
			"technician": {
				"first_name": "Frank",
				"last_name": "Carman",
				"employee_id": 1234,
				"id": 1
			},
			"id": 1
		},
	]
}

5. Create an appointment
Method: POST
URL:    http://localhost:8080/api/appointments/
Explanation:
This endpoint allows you to add a new appointment. A POST request with the following data structure will create a new appointment. (Please use the technician's automatically-generated id and not the custom employee_id to identify the technician.)
{
	"date_time": "2023-12-01T02:30:00+00:00",
	"reason": "Engine light on",
	"vin": "4T1BF3EK8BU168531",
	"customer": "Bob Doe",
	"technician": 1
}

6. Delete a specific appointment
Method: DELETE
URL: 	http://localhost:8080/api/appointments/:id/
Explanation:
This endpoint allows you to delete a specific appointment. Set ":id" in the above url to the appointment's id.  An empty DELETE request will delete that appointment.

7. Set specific appointment status to "canceled"
Method: PUT
URL:    http://localhost:8080/api/appointments/:id/cancel/
Explanation:
This endpoint allows you to set a specific appointment's status to "canceled". Set ":id" in the above url to the appointment's id. An empty PUT request will set that appointment's status to "canceled".

8. Set specific appointment status to "finished"
Method: PUT
URL:    http://localhost:8080/api/appointments/:id/finish/
Explanation:
This endpoint allows you to set a specific appointment's status to "finished". Set ":id" in the above url to the appointment's id. An empty PUT request will set that appointment's status to "finished".

### Sales API

The sales microservice handles all aspects of the operation related to purchasing a vehicle.

Relevant models include:
1. An AutomobileVO model containing a vin, sold, and an import href field. This value object polls data from the Automobile model listed in the inventory service.
2. A Salesperson model containing a first_name, last_name, and employee_id fields.
3. A customer model containing a first_name, last_name, address, and phone_number fields.
4. A sale model containing a price, automobile (foreign key to the AutomobileVO model), salesperson (foreign key to the Salesperson model), and customer (foreign key to the customer model) fields.

API Endpoints:

1. List Salespeople
Action:			List Salespeople
Method:			Get
URL:			http://localhost:8090/api/salespeople/
Explanation:
This endpoint allows you to view a list of all Salespeople, An empty GET request will fetch data with the following structure:
{
	"salespeople": [
		"first_name": "...", (str)
		"last_name": "...", (str)
		"employee_id": "...", (str)
		"id": "...", (int)
	]
}

2. Create Salesperson
Action: 		Creates a Salesperson
Method: 		Post
URL:			http://localhost:8090/api/salespeople/
Explanation:
This endpoint allows you to create a new salesperson: A POSt request with the following data structure will allow you to create a new salesperson:
{
	"first_name": "(input a string)",
	"last_name": "(input a string)",
	"employee_id": "(input a string)"
}

3. Delete a Salesperson
Action: 		Deletes a Salesperson
Method:			Delete
URL:			http://localhost:8090/api/salespeople/:id/
Explanation:
This endpoint allows you to delete a Salesperson by their specified ID in the url link.

4. List Customers
Action: 		Shows a list of all Customers
Method:			GET
URL: 			http://localhost:8090/api/customers/
Explanation:
This endpoint will list all of Customers. An empty GET request will fetch data with the following structure:
{
	"customers: [
		"first_name": "...",
		"last_name": "...",
		"address": "...",
		"phone_number": "...",
		"id": "..."
	]
}

5. Create a Customer:
Action: Creates a new Customer
Method: POST
URL: http://localhost:8090/api/customers/
Explanation:
This endpoint allows you to add a new Customer. A POST request with the following data will create a new Customer:
{
	"first_name": "(input a string)",
	"last_name": "(input a string)",
	"address": "(input a string)",
	"phone_number": "(input an integer)"
}
6. Delete a customer:
Action: 		Deletes a Customer
Method: 		DELETE
URL: 			"http://localhost:8090/api/customers/:id/"
Explanation:
This endpoint allows you to delete a Customer from the database. The customer id is required in the url after /customers/(insert the id of desired customer)/

7. List Sales
Action:			Shows a list of all Sales
Method:			GET
URL:			"http://localhost:8090/api/sales/"
Explanation:
This endpoint will show a list of all Sales. An empty GET request will fetch data with the following structure:
{
	"sales": [
		{
			"price": "...",		(int)
			"automobile": {
				"vin": "...",	(int)
				"sold": "..."	(boolean)
				"id": "..."		(int)
			},
			"salesperson": {
				"first_name": "...",	(str)
				"last_name": "...",		(str)
				"employee_id": "...",	(str)
				"id": "..."	(int)
			},
			"customer": {
				"first_name": "...",	(str)
				"last_name": "...",		(str)
				"address": "...",	(str)
				"phone_number": "...",	(int)
				"id": "..."		(int)
			},
			"id": "..."		(int)
		},
	]
}

8. Create a Sale
Action:		Creates a Sale
Method:		POST
URL:		"http://localhost:8090/api/sales/"
Explanation:
This endpoint will create a new Sale. A POST request with the following data will create a new Sale:
{
	"automobile": "(input vin number of desired automobile)",
	"salesperson": "(input id of salesperson)",
	"customer": "(input id of customer)",
	"price": "(input integer)"
}

9. Delete Sale
Action:		Deletes a Sales
Method:		DELETE
URL:		"http://localhost:8090/api/sales/:id/
Explanation:
This endpoint allows you to delete a Sale from the database using a DELETE request. Use the desired ID in the url to delete the Sale.

## Value Objects
1. Inventory Value Objects: Manufacturer, Vehicle model
2. Service Value Objects: AutomobileVO
3. Sales Value Objects: AutomobileVO
