from django.urls import path
from .views import api_list_technicians, api_technician_delete, api_list_appointments, api_appointment_delete, api_appointment_cancel, api_appointment_finish, api_list_automobileVOs


urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("service/automobile-vo/", api_list_automobileVOs, name="api_list_automobileVOs"),
    path("technicians/<int:pk>/", api_technician_delete, name="api_technician_details"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_appointment_delete, name="api_appointment_details"),
    path("appointments/<int:pk>/cancel/", api_appointment_cancel, name="api_appointment_cancel"),
    path("appointments/<int:pk>/finish/", api_appointment_finish, name="api_appointment_finish"),
]
