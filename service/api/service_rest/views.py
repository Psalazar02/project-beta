from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


# TECHNICIAN ENCODERS AND VIEWS

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianListEncoder
            )
        except:
            response = JsonResponse(
                {"message": "Could not load technicians"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_technician_delete(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"Message": "The specified technician does not exist"}
            )
            response.status_code = 404
            return response

# APPOINTMENT ENCODERS AND VIEWS

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentDetailEncoder,
            )
        except:
            response = JsonResponse(
                {"Message": "Could not load appointments"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            try:
                technician_id = content["technician"]
                technician = Technician.objects.get(id=technician_id)
                content["technician"] = technician
            except Technician.DoesNotExist:
                    response = JsonResponse(
                        {"Message": "The specified technician does not exist"}
                    )
                    response.status_code = 404
                    return response
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
                )
        except:
            response = JsonResponse(
                {"Message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_appointment_delete(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            response = JsonResponse(
                 {"Message": "The specified appointment does not exist"}
            )
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def api_appointment_finish(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse(
                {"Message": "The specified appointment does not exist"}
        )
        response.status_code = 404
        return response

@require_http_methods(["PUT"])
def api_appointment_cancel(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse(
                {"Message": "The specified appointment does not exist"}
                )
        response.status_code = 404
        return response

# AUTOMOBILEVO ENCODER AND VIEW

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
    ]

@require_http_methods(["GET"])
def api_list_automobileVOs(request):
    try:
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVOs": automobileVOs},
            encoder=AutomobileVOListEncoder,
        )
    except:
        response = JsonResponse(
            {"Message": "Could not load automobileVOs"}
        )
        response.status_code = 400
        return response
