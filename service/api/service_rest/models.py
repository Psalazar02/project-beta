from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobilevo", kwargs={"pk": self.id})

class Appointment(models.Model):
    STATUS_CHOICES = [
        ('created', 'Created'),
        ('finished', 'Finished'),
        ('canceled', 'Canceled'),
    ]
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.CharField(max_length=100)
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='created'
        )
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name='appointment',
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
