import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
from service_rest.models import AutomobileVO

def get_automobile():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    if response.status_code == 200:
        content = json.loads(response.content)
        print("Status:", response)
        for auto in content["autos"]:
            try:
                AutomobileVO.objects.update_or_create(
                    vin=auto["vin"],
                    defaults={
                        "sold": auto["sold"],
                    },
                )
            except:
                print("Could not update the AutomobileVO model")
    else:
        print("Status", response.status_code)


def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            get_automobile()
        except Exception as e:
            print(e, file=sys.stderr)
        if (not repeat):
            break
        time.sleep(60)

if __name__ == "__main__":
    poll()
